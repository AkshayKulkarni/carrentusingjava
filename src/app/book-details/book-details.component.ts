import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterServiceService } from '../Service/register-service.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  constructor(private act: ActivatedRoute, private router: Router, private registerservice: RegisterServiceService) { }
  carid;
  carDetails;
  car:any={};
  ngOnInit() {
    this.carid = this.act.snapshot.params['i'];
    this.registerservice.getCardetails().subscribe((cardetailsresponse:any)=>{
      this.carDetails = cardetailsresponse;
      console.log(this.carid);
      for(let i=0; i<=this.carDetails.length; i++) {
        if(this.carid == this.carDetails[i].carid){
          this.car = this.carDetails[i];
          break;
        }
      }
    });
    console.log(this.carid);
  }

}

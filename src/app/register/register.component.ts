import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { RegisterServiceService } from '../Service/register-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private registerService: RegisterServiceService) { }
  registerDetails;
  registerForms = new FormGroup ({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    emailid: new FormControl(''),
    pass: new FormControl(''),
  });
  ngOnInit() {
  }
  register() {
    this.registerDetails = this.registerForms.value;
    this.registerService.sendRegisterDetails(this.registerDetails).subscribe((msg)=>{console.log(msg); });
    console.log(this.registerForms.value);
  }
}

import { Component, OnInit } from '@angular/core';
import { RegisterServiceService } from '../Service/register-service.service';

@Component({
  selector: 'app-car-view',
  templateUrl: './car-view.component.html',
  styleUrls: ['./car-view.component.css']
})
export class CarViewComponent implements OnInit {

  constructor(private registerService: RegisterServiceService) { }
  responseData: any[] = [];
  ngOnInit() {
    this.view();
  }
  view(){
    this.registerService.getCardetails().subscribe((carDetails:any)=>this.responseData=carDetails);
  }
}

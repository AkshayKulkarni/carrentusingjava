import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegisterServiceService {

  constructor(private http: HttpClient) { }
  Url= "";
  sendRegisterDetails(registerDetails) {
    console.log(registerDetails);
    return this.http.post('http://localhost:8080/JavaCARProject/UserRegister', registerDetails);
  }
  sendCarDetails(addcarDetails) {
    console.log(addcarDetails);
    return this.http.post('http://localhost:8080/JavaCARProject/Addcar', addcarDetails);
  }
  getCardetails() {
    return this.http.get('http://localhost:8080/JavaCARProject/carView');
  }
  loginDetails(loginDetails) {
    console.log(loginDetails);
    return this.http.post('http://localhost:8080/JavaCARProject/loginUser', loginDetails);
  }
  // carbookDetails(id){
  //   return this.http.post('http://localhost:8080/JavaCARProject/carBookid', id);
  // }
}

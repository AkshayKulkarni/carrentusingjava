import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { RegisterServiceService } from '../Service/register-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private registerservice: RegisterServiceService, private router: Router) { }
  loginDetails;
  logingmsg: any = {};
  loginForms = new FormGroup({
    emailid: new FormControl(''),
    pass: new FormControl('')
    // emailid: new FormControl('', [Validators.required, Validators.email]),
    // pass: new FormControl('', [Validators.required]),
  });
  ngOnInit() {
  }
  login() {
    this.loginDetails = this.loginForms.value;
    this.registerservice.loginDetails(this.loginDetails).subscribe((msg)=> {
    this.logingmsg = msg;
    if (this.logingmsg.msg =='done') {
      console.log('login success');
      this.router.navigate(['/home']);
    } else if (this.logingmsg.msg =='adminSuccess') {
      console.log('admin login success');
    } else {
        console.log('login not success');
    }

    });
    console.log(this.loginForms.value);
  }
}

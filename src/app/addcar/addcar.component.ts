import { Component, OnInit } from '@angular/core';
import { RegisterServiceService } from '../Service/register-service.service';

@Component({
  selector: 'app-addcar',
  templateUrl: './addcar.component.html',
  styleUrls: ['./addcar.component.css']
})
export class AddcarComponent implements OnInit {

  constructor(private registerService: RegisterServiceService) { }
  model:any={};
  addcarDetails: any;
  selectedFiles:File=null;
    onFileSelected(File: FileList) {
      this.selectedFiles = File.item(0);
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.model.carimage = event.target.result;
    };
      reader.readAsDataURL(this.selectedFiles);
    }
  addcar() {
    this.addcarDetails = this.model;
    this.registerService.sendCarDetails(this.addcarDetails).subscribe((msg)=>{console.log(msg); });
    console.log(this.model);
  }
  ngOnInit() {
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from "@angular/router";
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { AddcarComponent } from './addcar/addcar.component';
import { CarViewComponent } from './car-view/car-view.component';
import { HomeComponent } from './home/home.component';
import { CarViewsideComponent } from './car-viewside/car-viewside.component';
import { BookDetailsComponent } from './book-details/book-details.component';
const route: Routes = [
  {path: '', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'addcar', component: AddcarComponent},
  {path: 'carview', component: CarViewComponent},
  {path: 'home', component: HomeComponent},
  {path: 'rentcars', component: CarViewsideComponent},
  {path: 'bookDetails/:i', component: BookDetailsComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AddcarComponent,
     CarViewComponent,
     HomeComponent,
     CarViewsideComponent,
     BookDetailsComponent

  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule, ReactiveFormsModule, RouterModule.forRoot(route)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

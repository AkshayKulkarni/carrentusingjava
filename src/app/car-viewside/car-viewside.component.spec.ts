import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarViewsideComponent } from './car-viewside.component';

describe('CarViewsideComponent', () => {
  let component: CarViewsideComponent;
  let fixture: ComponentFixture<CarViewsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarViewsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarViewsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { RegisterServiceService } from '../Service/register-service.service';
@Component({
  selector: 'app-car-viewside',
  templateUrl: './car-viewside.component.html',
  styleUrls: ['./car-viewside.component.css']
})
export class CarViewsideComponent implements OnInit {

  constructor(private registerService: RegisterServiceService) { }
  carviewData: any[] = [];
  ngOnInit() {
    this.view();
  }
  view() {
    this.registerService.getCardetails().subscribe((carDetails: any)=>this.carviewData = carDetails);
    // this.registerService.getCardetails().subscribe((carDetails:any)=>this.carViewData=carDetails))
  }
}
